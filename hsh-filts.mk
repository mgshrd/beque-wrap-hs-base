src/Data/Char.v: src/Data/Char.hsh.filt
	cat $< | grep -v ^'--' | $(BINDGENHS) $(notdir $*) >$@.tmp
	rm -f $@
	if grep -q ' IO ' $@.tmp; then \
		echo "Require Beque.Util.result.\\nRequire Strings.String.\\nRequire Beque.IO.impl.AxiomaticIOInAUnitWorld.\\nDefinition IO' := Beque.IO.impl.AxiomaticIOInAUnitWorld.AxiomaticIOInAUnitWorld.IO.\\nDefinition msg' := Beque.IO.impl.AxiomaticIOInAUnitWorld.UnitWorld.msg." >> $@; \
	fi
	if grep -q 'Extract' $@.tmp; then \
		echo "From Coq Require Extraction." >> $@; \
	fi
	if grep -q 'Beque.Util.inj_pair2b.' $@.tmp; then \
		echo "Require Beque.Util.inj_pair2b." >> $@; \
	fi
	if grep -q 'Beque.Util.dec_utils.' $@.tmp; then \
		echo "Require Beque.Util.dec_utils." >> $@; \
	fi
	if [ -e $*.v.deps ]; then \
		sed -e 's/.*/Require Import Haskell.&./' < $*.v.deps >> $@; \
	fi
	if [ -e $*.v.pre ]; then \
		cat $*.v.pre >> $@; \
	fi
	cat $@.tmp >> $@
	if [ -e $*.v.post ]; then cat $*.v.post > $@; fi
	rm $@.tmp

src/Data/Char.hsh.filt: src/Data/Char.hsh
	cat $< | \
	sed -e 's/ShowS/(String -> String)/g' | \
	sed -e 's/ReadS String/(String -> [(String, String)])/g' | \
	sed -e 's/ReadS Char/(String -> [(Char, String)])/g' | \
	cat > $@

src/Prelude.hsh.filt: src/Prelude.hsh
	cat $< | \
	sed -e 's/forall.*\.//' | \
	grep -v "^undefined " | \
	grep -v "^error " | \
	grep -v "^data Bool " | \
	grep -v "^data IO " | \
	grep -v "^data Maybe " | \
	grep -v "^data Either " | \
	cat > $@

src/Control/Exception/Base.hsh.filt: src/Control/Exception/Base.hsh
	cat $< | \
	sed -e 's/forall.*\.//' | \
	grep -v "^data SomeAsyncException " | \
	grep -v "^data SomeException " | \
	grep -v "Addr#" | \
	grep -v " ThreadId " | \
	grep -v " ErrorCall " | \
	sed -e 's/IOError/IOException/g' | \
	cat > $@

src/System/IO.hsh.filt: src/System/IO.hsh
	cat $< | \
	grep -v "^data IO " | \
	cat > $@

src/Text/Read.hsh.filt: src/Text/Read.hsh
	cat $< | \
	sed \
	-e "s/Char Char/Char' Char/" \
	-e "s/String String/String' String/" \
	-e "s/Number Number/Number' Number/" | \
	cat > $@
